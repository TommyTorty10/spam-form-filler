import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from random import randrange

def firefox_tor(ip='127.0.0.1', port=9050):
    fp = webdriver.FirefoxProfile()
    fp.set_preference("network.proxy.type", 1)
    fp.set_preference("network.proxy.socks", ip)
    fp.set_preference("network.proxy.socks_port", port)
    fp.update_preferences()
    return webdriver.Firefox(firefox_profile=fp)

def firefox_clear():
    return webdriver.Firefox()

def check_tor(browser):
    """check if tor is workng by visiting the check page on their website"""
    browser.get("https://check.torproject.org/")

def fill_form(url, email, browser, delays=2):
    # check_tor(browser)
    browser.get(url)
    time.sleep(delays)

    body = browser.find_element_by_class_name('freebirdLightBackground ')
    textboxes = browser.find_elements_by_class_name("quantumWizTextinputPaperinputInput")
    radiobuttons = browser.find_elements_by_class_name("docssharedWizToggleLabeledLabelWrapper")
    checkboxes = browser.find_elements_by_class_name("quantumWizTogglePapercheckboxInnerBox")
    bubbles = browser.find_elements_by_class_name('freebirdFormviewerComponentsQuestionRadioChoice freebirdFormviewerComponentsQuestionRadioOptionContainer')
    submitbutton = browser.find_element_by_class_name("appsMaterialWizButtonPaperbuttonContent")
    # submitbutton = browser.find_elements_by_xpath('//*[@id="mG61Hd"]/div[2]/div/div[3]/div[1]/div/div/span/span')

    for t in textboxes:
        t.send_keys(email)

    # for t in range(len(textboxes)):
    #     body.send_keys('Welcome to the Jungle!')
    #     body.send_keys(Keys.TAB)

    # for b in radiobuttons:
    #     b.click()
    # radiobuttons[-1].click()

    # for c in checkboxes:
    #     c.click()

    # for b in bubbles:
    #     b.click()

    s = list('Welcome to the Jungle!')
    chars = """UVWXYZ1234567890!@#$%^&*()[]\{}|;',./:"<>?"""
    for i in range(4):
        body.send_keys(Keys.TAB)
        sindex = randrange(0, len(s))
        charindex = randrange(0, len(chars))
        s[sindex] = chars[charindex]
        body.send_keys(''.join(s))

    submitbutton.click()
    time.sleep(delays)
    browser.close()
