# Spam Form Filler
Ever gotten a spam messge with a Google Form and wish you could fight back?  Given a google form,
this program will fill out the form with spam and submit it.  
Do not use this program to harass people or abuse people or systems.
I, the developer, am in no way responsible for your use of this software.
It is intended for research and fighting abuse only.

## Requirements
* Tor Linux package
  * `apt install tor`
* Firefox
  * You may have to install [geckodriver.](https://github.com/mozilla/geckodriver/releases)
  * Other browsers have other requirements and nuances and are not supported.
* Python 3.8
* Python packages in requirements.txt
  * `pip3 install -r requirements.txt`

# Usage
The `form_filler.py` file has some general examples of how to access a form.
It is meant to serve as a module that can be expanded or duplicated and modified.
The `main.py` file shows how to use the module.  
If `main.py` is run as is, it will open up the form given in the link, fill out the form, submit it,
close the browser window, and sleep for a random interval between 0 and 60 seconds before repeating the whole process (I know it's more than seconds but idk why ¯\_(ツ)_/¯ ). 
The process will be repeated indefinitely. If you want a burner email for this program to use,
check out [Email on Deck.](https://www.emailondeck.com/)  

# License
Anyone is free to use, copy, and modify this software within the following restrictions.
You may not use this to abuse Google Forms, harass people, or any illegal activity.
The purpose of this software is for research into spam and abuse and for combating spam bots that utilize Google Forms for abuse
and theft.
