from random import randrange
from form_filler import *

if __name__ == "__main__":
    # these are the ip and port tor is accessed through via a socks5 proxy
    ip = '127.0.0.1'
    port = 9050
    email = 'bart2@xwkqguild.com'  # burner emails can be made with https://www.emailondeck.com/
    # url = 'https://docs.google.com/forms/d/e/1FAIpQLSfOP8AN1NQZQ6GlP7OXbdyOJTTpYiD12HP1QrXXUfOf_bvV_g/viewform'
    url = 'https://docs.google.com/forms/d/e/1FAIpQLSe8DT0_3YXtuOV5J724ovhHAVhXc2TO3fuJV62MoH5qEH_LLQ/viewform'
    # url = 'https://docs.google.com/forms/d/e/1FAIpQLSdPuLDwDDQBJF5xCcUKkgQldC5_MJqMRgR5svQtMraymN9FwQ/viewform'

    while True:
        browser = firefox_tor(ip, port)
        fill_form(url, email, browser)
        time.sleep(1)
        # browser.quit()
        del browser

        # r = randrange(0, 60)  # todo for some reason time.sleep last more than r seconds
        r = 5  # short value for testing purposes.  IDK if google forms would catch a lot of quick (as quick as possible) requests,
                 # but I don't intend to push the limit.
        for i in range(r):
            print(r - i)
            time.sleep(i)
        print('-' * 24)
